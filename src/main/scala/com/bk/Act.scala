package com.bk

import scala.collection.immutable.IndexedSeq
import util.Random
import org.joda.time.DateTime
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import collection.JavaConverters._

object Activity extends App {
  val utf8:Charset = StandardCharsets.UTF_8
  val now = new DateTime()
  val lnow: Long = now.getMillis

  val r = new Random()

  val range = 1 to 10000
  val things: IndexedSeq[String] = range.map(n => {
    val randMillis = r.nextInt(60 * 1000)
    val fakeTime = lnow + randMillis
    s"${r.nextInt(25)} $fakeTime ${"x" * r.nextInt(50)}"
  })


  val lines:List[String] = things.toList
  // This seems to expect java.util.List - use JavaConverter to write list items to the file.
  Files.write(Paths.get("activity.txt"), lines.asJava, utf8)

  lines.foreach(println(_))
}





